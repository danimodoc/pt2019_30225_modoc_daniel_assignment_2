package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import model.CashCheck;
import model.Client;
import model.SuperMarket;
import view.QueueView;

public class QueueControl {//implements ActionListener, Runnable 
	
	private List<JTextField> queues = new ArrayList<JTextField>();
	private QueueView view ;
	
	public QueueControl(QueueView mview) {//SuperMarket s,CashCheck c
		view=mview;
		view.addStartListener(new StartListener());
	}
	
	class StartListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			String minAr="";
			String maxAr="";
			String minSv="";
			String maxSv="";
			String simu="";
			String nrQu="";

			try {
				minAr=view.getMinAvTime();
				maxAr=view.getMaxAvTime();
				minSv=view.getMinServTime();
				maxSv=view.getMaxServTime();
				simu=view.getSimTime();
				nrQu=view.getNumQueues();
				
				CashCheck[] cashChecks = new CashCheck[Integer.parseInt(nrQu)];
				for(int i=0; i<Integer.parseInt(nrQu); i++){
					cashChecks[ i ] = new CashCheck("Cashcheck "+Integer.toString( i ),view);
					cashChecks[ i ].start();
				} 
				SuperMarket p = new SuperMarket(Integer.parseInt(nrQu),"",cashChecks,Integer.parseInt(minAr),Integer.parseInt(maxAr),Integer.parseInt(minSv),Integer.parseInt(maxSv),Integer.parseInt(simu),view);
				p.start();				

			} catch (NumberFormatException nfex) {
				//m_view.showError("Bad input: '" + userInput + "'");
			}
		}
	}
}
