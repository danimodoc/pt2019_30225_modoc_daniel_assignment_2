package view;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import model.CashCheck;
import model.Client;
import model.SuperMarket;
import view.QueueView;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;

import controller.QueueControl;
import model.CashCheck;
import model.SuperMarket;

public class QueueView extends JFrame {
	
	private JFrame f = new JFrame("Priority Queues");
	private JTextField minArrival = new JTextField(5);
	private JTextField maxArrival = new JTextField(5);
	private JTextField minServTime = new JTextField(5);
	private JTextField maxServTime = new JTextField(5);
	private JTextField simInterval = new JTextField(5);
	private JTextField numQueues = new JTextField(5);
	
	private JTextField peakHour = new JTextField(5);
	private JTextField avWait = new JTextField(5);
	private JTextField avServ = new JTextField(5);
	private JTextField emptyTime = new JTextField(5);
	
	private JPanel content7 = new JPanel();
	private JPanel content8 = new JPanel();
	private JPanel content9 = new JPanel();
	private JPanel content10 = new JPanel();
	private JPanel content11 = new JPanel();
	
	JPanel contentAll = new JPanel();
	
	private List<JLabel> queues = new ArrayList<JLabel>();
	private LinkedList<JLabel>[] clients;//
	
	private JButton start = new JButton("Start");
	private SuperMarket superMarket;
	private CashCheck cashCheck;
	
	public QueueView(SuperMarket s,CashCheck c) {
		
		//JFrame f = new JFrame ("Priority Queues");
		this.superMarket=s;
		this.cashCheck=c;
		
		clients = new LinkedList[5];
		
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(500,500); 	
		
		JPanel content1 = new JPanel();
		//content1.setLayout(new GridLayout(3,2));
		content1.setLayout(new FlowLayout());
		content1.add(new JLabel("Minimum Arrival            "));
		content1.add(minArrival);
		
		content1.add(new JLabel("Maximum Arrival             "));
		content1.add(maxArrival);
		
		JPanel content2 = new JPanel();
		content1.setLayout(new FlowLayout());
		content2.add(new JLabel("Minimum service time "));
		content2.add(minServTime);
		
		content2.add(new JLabel("Maximum service time    "));
		content2.add(maxServTime);
		
		JPanel content3 = new JPanel();
		content1.setLayout(new FlowLayout());
		content3.add(new JLabel("Number of queues          "));
		content3.add(numQueues);
		
		content3.add(new JLabel("Simulation time                "));
		content3.add(simInterval);
		
		JPanel content4 = new JPanel();
		content4.setLayout(new FlowLayout());
		
		content4.add(new JLabel("Peak hour                        "));
		content4.add(peakHour);
		peakHour.setEditable(false);
		
		content4.add(new JLabel("Average Wait                      "));
		content4.add(avWait);
		avWait.setEditable(false);
		
		JPanel content5 = new JPanel();
		content5.setLayout(new FlowLayout());
		
		content5.add(new JLabel("Average Service            "));
		content5.add(avServ);
		avServ.setEditable(false);
		
		content5.add(new JLabel("Empty time                         "));
		content5.add(emptyTime);
		emptyTime.setEditable(false);
			
		JPanel content6 = new JPanel();
		content6.add(start);
	
		queues.add(new JLabel("             Queue1 :                                               "));//JTextField(23)
		queues.add(new JLabel("             Queue2 :                                               "));
		queues.add(new JLabel("             Queue3 :                                               "));
		queues.add(new JLabel("             Queue4 :                                               "));
		queues.add(new JLabel("             Queue5 :                                               "));
		
		for(int i=0;i<5;i++)
		{
			clients[i] = new LinkedList<JLabel>();
		}
		
		start.setPreferredSize(new Dimension(240,70));
		
		content7.add(queues.get(0));
		content8.add(queues.get(1));
		content9.add(queues.get(2));
		content10.add(queues.get(3));
		content11.add(queues.get(4));
		
		contentAll.add(content1);
		contentAll.add(content2);
		contentAll.add(content3);
		contentAll.add(content4);
		contentAll.add(content5);
		contentAll.add(content6);
		contentAll.add(content7);
		contentAll.add(content8);
		contentAll.add(content9);
		contentAll.add(content10);
		contentAll.add(content11);
		
		f.setContentPane(contentAll);
		f.setVisible(true);
	}
	
	public String  getMinAvTime() {
		return minArrival.getText();
	}
	
	public String  getMaxAvTime() {
		return maxArrival.getText();
	}
	
	public String  getMinServTime() {
		return minServTime.getText();
	}
	
	public String  getMaxServTime() {
		return maxServTime.getText();
	}
	
	public String getSimTime() {
		return simInterval.getText();
	}
	
	public String getNumQueues() {
		return numQueues.getText();
	}
	
	public void setAvTime(String s) {
		avWait.setText(s);
	}
	
	public void setAvServ(String s) {
		avServ.setText(s);
	}
	
	public void setPeakHour(String s) {
		peakHour.setText(s);
	}
	
	public void setEmptyTime(String s) {
		emptyTime.setText(s);
	}
	
	public void addStartListener(ActionListener mal) {
		start.addActionListener(mal);
	}
	
	public void queueClient(int CashCheckIndex) {
		
		JLabel c = new JLabel("C ");
		clients[CashCheckIndex].add(c);
		
		if(CashCheckIndex == 0)
			content7.add(clients[CashCheckIndex].getLast());//queues.get(0).setText(((clients[CashCheckIndex].getLast().getText())));
		else if(CashCheckIndex == 1)
			content8.add(clients[CashCheckIndex].getLast());
		else if(CashCheckIndex == 2)
			content9.add(clients[CashCheckIndex].getLast());
		else if(CashCheckIndex == 3)
			content10.add(clients[CashCheckIndex].getLast());
		else if(CashCheckIndex == 4)
			content11.add(clients[CashCheckIndex].getLast());

		f.setContentPane(contentAll);
	}
	
	public void dequeueClient(int CashCheckIndex) {
		if(CashCheckIndex == 0)
		{
			content7.remove(clients[CashCheckIndex].getLast());
			clients[CashCheckIndex].removeLast();
		}
		else if(CashCheckIndex == 1)
		{
			content8.remove(clients[CashCheckIndex].getLast());
			clients[CashCheckIndex].removeLast();
		}
		else if(CashCheckIndex == 2)
		{
			content9.remove(clients[CashCheckIndex].getLast());
			clients[CashCheckIndex].removeLast();
		}
		else if(CashCheckIndex == 3)
		{
			content10.remove(clients[CashCheckIndex].getLast());
			clients[CashCheckIndex].removeLast();
		}
		else if(CashCheckIndex == 4)
		{
			content11.remove(clients[CashCheckIndex].getLast());
			clients[CashCheckIndex].removeLast();
		}

		f.setContentPane(contentAll);
	}
}
