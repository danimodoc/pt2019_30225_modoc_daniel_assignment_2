package model;

public class Client {

	private int arriveTime;
	private int serviceTime;
	private int nr;
	private int queueNr;
	
	public Client(int nr,int aTime, int sTime,int queueNr) {
		this.nr=nr;
		this.arriveTime = aTime;
		this.serviceTime = sTime;
		this.queueNr=queueNr;
	}

	public int getArriveTime() {
		return this.arriveTime;
	}

	public int getServiceTime() {
		return this.serviceTime;
	}
	
	public int getNr() {
		return this.nr;
	}

	public void setArriveTime(int aTime) {
		this.arriveTime = aTime;
	}
	
	public void setQueueNr(int nr)
	{
		this.queueNr=nr;
	}
	
	public int getQueueNr()
	{
		return this.queueNr;
	}
}
