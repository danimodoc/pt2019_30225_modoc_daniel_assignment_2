package model;

import controller.QueueControl;
import view.QueueView;

public class MainClass {
	
	public static void main(String[] args) {
		
		SuperMarket s= new SuperMarket();
		CashCheck c = new CashCheck();
		QueueView view = new QueueView(s,c);
		QueueControl control = new QueueControl(view);
    }
}
