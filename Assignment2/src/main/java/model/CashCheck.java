package model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import javax.swing.JTextField;

import view.QueueView;

public class CashCheck extends Thread {

	private volatile ArrayList<Client> clients = new ArrayList<Client>();
	private volatile int totalWaitingPeriod;
	QueueView view;
	private static int peakH=0;
	private static int emptyTime=0;
	
	public CashCheck(String name) {// ,List<JTextField> queues
		  //this.queues=queues;
          setName(name);
	}
	
	public CashCheck() {
	}
	
	public CashCheck(String name,QueueView q) {//
        setName(name);
        this.view = q;
	}
	
	public synchronized void addClient(Client c) {//+ Loggerul , update
		clients.add(c);
		peakH++;
		if(clients.size() == 0)
			emptyTime++;
		view.setEmptyTime(Integer.toString(emptyTime));
		
		view.setPeakHour(Integer.toString(peakH));
		notifyAll();
	}
	
	public synchronized Client eraseClient() throws InterruptedException {
		while(clients.size() == 0)
			wait();
		
		Client c = clients.get(0);
		int n=clients.remove(0).getQueueNr();
		totalWaitingPeriod = totalWaitingPeriod - c.getServiceTime();
		view.dequeueClient(n);
		peakH--;
		view.setPeakHour(Integer.toString(peakH));
		
		if(clients.size() == 0)
			emptyTime++;
		view.setEmptyTime(Integer.toString(emptyTime));
		
		System.out.println("Client "+Long.toString(c.getNr())+" left "+getName());
		
		notifyAll();
		return c;
	}

	@Override
	public void  run() {
		try
		{
			while(true) {
				Client c = eraseClient();			
				sleep((int) (Math.random()*4000));//(int) (Math.random()*4000)n*1000
			}
		}
		catch(InterruptedException e)
		{
			System.out.println( e.toString());
		}
	}
	
	public synchronized int getTotalWaitingTime() {
		return this.totalWaitingPeriod;
	}

	public synchronized ArrayList<Client> getClients() {
		return clients;
	}
	
	public String toString() {
		String s="";
		for(Client c:clients) {
			s=s+" "+c.getNr();
		}
		return s;
	}
}
