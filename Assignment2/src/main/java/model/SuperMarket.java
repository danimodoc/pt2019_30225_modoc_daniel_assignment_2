package model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;

import view.QueueView;

public class SuperMarket extends Thread {
	
	private volatile List<CashCheck> cashChecks = new ArrayList<CashCheck>();//= new ArrayList<CashCheck>() ;
	private int nr_checks;
	private int count=0;
	//private int total_clients;
	private int nr=0;
	private int minArr;
	private int maxArr;
	private int minServ;
	private int maxServ;
	private int simulationTime;
	private QueueView view;

	public SuperMarket() {}
	
	public SuperMarket(int nr_ch,String name,CashCheck[] List,int x,int y,int a,int b,int sim,QueueView view) {//ArrayList<CashCheck> List , List<JTextField> queues
		setName(name);
		this.nr_checks=nr_ch;
		this.cashChecks = new ArrayList<CashCheck>(nr_ch) ;
        for(int i=0;i<nr_ch;i++)
           this.cashChecks.add(List[i]);	
        this.minArr=x;
        this.maxArr=y;
        this.minServ=a;
        this.maxServ=b;
        this.simulationTime=sim;
        this.view=view;
        //this.queues = queues;
	}
	
	public int getMinCheck() {
		int i=0;
		int min = cashChecks.get(0).getClients().size();
		int k=0;
		
		for(CashCheck m :cashChecks)
		{
			int aux=m.getClients().size();
			if(aux < min)
			{
				min = aux;
				i=k;	
			}	
			k++;
		}
		return i;
	}
	
	public synchronized List<CashCheck> getCashChecks() {
		return cashChecks;
	}
	
	public void run() {
		try
		{
			int i=0;
			
			int peak=0;
			
			int countClients =0 ;
			int servSum=0;
			int avArrSum=0;
		    
			for(int arrTime = (int) (Math.random() * (maxArr - minArr)) + minArr; arrTime < simulationTime;arrTime+= (int) (Math.random() * (maxArr - minArr)) + minArr)//
			{
				int servTime = (int) (Math.random() * (maxServ - minServ)) + minServ;
				i++;
				
				avArrSum=avArrSum+arrTime;
				
				int minQueue=getMinCheck();
				Client c = new Client(++nr,arrTime,servTime,minQueue);	
				
				countClients++;
				
				servSum+=c.getServiceTime();
				
				System.out.println("Client " +Integer.toString(nr)+" added at cashcheck "+ Integer.toString(minQueue));
				c.setQueueNr(minQueue);
				
				view.queueClient(minQueue);
				
				cashChecks.get(minQueue).addClient(c);
				
				sleep(servTime*150);//(int) (Math.random()
			}
			
			double averageService = (double)servSum/countClients;
			double averageArrival = ((double)avArrSum/countClients)/10;
			
			view.setAvServ(Double.toString(averageService));
			view.setAvTime(Double.toString(averageArrival));
			
			System.out.println("Average service is "+averageService);
			System.out.println("Average arrival time is "+avArrSum);
			System.out.println("There were in total "+countClients+" clients");
			
		}
		catch(InterruptedException e)
		{
			System.out.println(e.toString());
		}
		
		
	}
}

